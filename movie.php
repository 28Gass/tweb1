<!DOCTYPE html>
<!--
(Luca Scutti , Corso B, matricola 867778 )
-->
<html lang="it">

<?php
#in questa prima parte di php eseguiamo la get del film che vogliamo visualizzare nella
#pagina in seguito leggiamo il file di testo info.txt in cui sono presenti tutte le info
#del nostro film
 $film = $_GET['film'];
$linee = file($film . "/info.txt", FILE_IGNORE_NEW_LINES);
list($titolo, $anno, $feed) = $linee;//per ogni righa del file di testo corrisponde ad titolo, anno , valutazione
?>
<head>
    <title><?= "$titolo" ?> - Rancid Tomatoes</title>
    <link href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type="image/gif" rel="icon">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="movie.css" type="text/css" rel="stylesheet" />
</head>

<body>
<div id = 'testa'>
    <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes" />
</div>

<h1><?= "$titolo ($anno)" ?> </h1>
<div id="TAB">
    <div id="destra">
        <div>
 <img src="<?= $film ?>/overview.png" alt="general overview" />
        </div>
        <div id="generali">
            <?php
           //le
    foreach (file($film . "/overview.txt") as $overview) {//facciamo un foreach della overview.txt dove sono presenti le generalità del film
    list($contesto, $testo) = explode(":", $overview);
    //dividiamo le varie categorie con la propria descrizione cioè DISTRIBUTORS(va nel contesto) e la sua descrizione in testo
    //per effettuare questa separazione usiamo explode : che separerà la stringa quando incontra :
                ?>
                <dl>
                    <dt><?= "$contesto" ?></dt> #per stampare correttamente il contesto e il testo
                    <dd><?= "$testo" ?></dd>
                </dl>
            <?php } ?>
        </div></div>
    <?php
    $recensioni = glob($film . "/review*.txt");//variabili globali di tutte le recensioni
    $TotRec = count($recensioni); //effettuo la conta del lenght dell'array recensioni
    $NumRec = $TotRec; ?>

    <div id="sinistra">
        <div id="sing">
            <img  src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rottenbig.png" alt="Rotten" />
            <strong id="titolo"><?= "$feed"?>%</strong>
        </div>
        <?php
        if ($TotRec > 10) {//se abbiamo un numero >10 recensioni
            $NumRec = 10; // NumRec sarà di 10 così visualizzeremo solo "10"
        }
        if ($NumRec % 2 == 0) {//se le recensioni sono pari
            $half = $NumRec / 2;//le dividiamo in modo uguale
        } else {
            $half = (int) ($NumRec / 2) + 1;//nel caso in cui siano dispari
        }
        for ($i = 0; $i < $NumRec; $i++) {//ciclo for ci permette di stampare le recensioni a destra o a sinistra
        if ($i == 0) {//per sceglire dove stampare la recensione
        ?>
        <div id="colonnasinistra">
            <?php
            } else if ($i == $half) {
            ?>
        </div>
        <div id="colonnadestra">
            <?php
            }
            list($punteggio,$feedback,$persona,$date) =
                file($recensioni[$i], FILE_IGNORE_NEW_LINES);//leggiamo i vari dati riguardanti la recensione (nome,feedback ecc...)
            ?>

            <p class="feed">
                <img class="gif" src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?= strtolower($feedback) ?>.gif" alt="<?= ucfirst(strtolower($feedback)) ?>"/>
                <q><?= "$punteggio" ?></q>
            </p>
            <p>
                <img class="gif" src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic" />
                <?= "$persona" ?> <br />
                <span><?= "$date" ?></span>
            </p>
            <?php } ?>
        </div>
    </div>
    <p id="sotto">(1-<?= "$NumRec" ?>) of <?= "$TotRec" ?></p>
</div>
<div id ="validatore">
    <a href="ttp://validator.w3.org/check/referer"><img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML"></a> <br>
    <a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
</div>
</body>
</html>
